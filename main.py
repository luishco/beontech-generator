from generator.generator import stock_trade_stream


def main():
    process_large_trades(stock_trade_stream)


def process_large_trades(_generator):
    for event in _generator():
        print(event)


main()
