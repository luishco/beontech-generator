from typing import Dict
from generator.models import Trade, Event, Partition
import time

import random


def stock_trade_stream() -> Dict:
    while True:
        time.sleep(0.01)

        partition = random.choice(list(Partition))
        trades_list = [
            Trade.new_trade().dict() for _ in range(0, random.randint(1, 10))
        ]
        event = Event(
            trades=trades_list,
            partition=partition,
        )
        yield event
