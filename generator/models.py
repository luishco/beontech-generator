from enum import Enum

from typing import List
from decimal import Decimal
from pydantic import BaseModel

import random


class Partition(Enum):
    zero: str = "0"
    one: str = "1"
    two: str = "2"


class Trade(BaseModel):
    symbol: str
    price: Decimal
    quantity: int

    @classmethod
    def new_trade(cls):
        price = Decimal(random.randrange(1, 100))
        quantity = random.randint(1, 1000)
        return Trade(symbol="WIN", price=price, quantity=quantity)


class Event(BaseModel):
    trades: List["Trade"]
    partition: Partition
