# Coding Interview

```This code was made during Beon.tech coding interview```


### Task 1 - Generator:
    Write a Python generator function named stock_trade_stream that simulates a stream of stock trade events. Each event should be a dictionary with fields for “symbol”, “price”, and “quantity”. The function should indefinitely yield random trade events.

### Task 2 - Processing an Event Stream:
    Write a Python function named process_large_trades that takes the generator from the first exercise as an argument. The function should iterate over the event stream and print a message for each trade where the quantity is more than 100.

### Task 3 - Partitions:
    Extend the stock_trade_stream generator function from the previous exercise to simulate the concept of partitions. Instead of yielding individual events, it should now yield lists of events, where each list represents a partition. Each event should now also have a “partition” field.

### Task 4 - MultipleTradeConsumer: 
    Create multiple TradeConsumer instances with different consumer group ids to simulate a consumer group. Run them concurrently to process the event stream.
